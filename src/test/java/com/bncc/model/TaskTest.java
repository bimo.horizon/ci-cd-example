package com.bncc.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {
    @Test
    public void testGetStatusExpectedNotDone() {
        Task task = new Task(1, "Push up", "Health");
        String expectedStatus = "NOT DONE";

        String actualStatus = task.getStatus();

        assertEquals(expectedStatus, actualStatus);
    }

    @Test
    public void testGetTask() {
        Task task = new Task(1, "Push up", "Health");
        String expectedTask = "1. Push up [NOT DONE]";

        String actualTask = task.getTask();

        assertEquals(expectedTask, actualTask);
    }

    @Test
    public void testIsTaskExpectedTrue() {
        Task task = new Task(2, "Sky diving", "Fun");

        boolean isTaskFound = task.isTask(2);

        assertTrue(isTaskFound);
    }

    @Test
    public void testMarkTaskExpectedDone() {
        Task task = new Task(1, "Push up", "Health");
        String expectedStatus = "DONE";

        task.markTaskDone();
        String actualStatus = task.getStatus();

        assertEquals(expectedStatus, actualStatus);
    }
}